==================
Contact us
==================

E-mail: hpc@narit.or.th

.. |slack-img| image:: slack.png 
    :target: https://join.slack.com/t/chalawan/signup
    :alt: Joining Chalawan Workspace
    :height: 30px
    :width: 30px

|slack-img| : `Join <https://join.slack.com/t/chalawan/signup>`_ Slack `Chalawan Workspace <https://chalawan.slack.com>`_ for support