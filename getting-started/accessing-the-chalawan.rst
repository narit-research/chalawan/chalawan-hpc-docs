==================
Accessing the Chalawan
==================

The command-line interface
=========================================================
Our operating system is based on GNU/Linux. Thus, a command-line interface or command language interpreter (CLI) is the primary mean of interaction with our HPC. In case you are not familiar with the command-line interface, free-online course at Codecademy is a good place to start.

Accessing the Chalawan
=========================================================

For Microsoft Windows user, see Connect to the Remote Server from Microsoft Windows.

The Chalawan cluster is an isolated system which resides in the NARIT’s internal network. At the present time, we have two systems, Castor and Pollux (hereafter the computing systems).

Castor is the old system which is assigned with the IP address 192.168.5.100. It contains 16 traditional Compute nodes suited for CPU-intensive tasks.
Pollux is the newest one assigned with the IP address 192.168.5.105. It contains 3 GPU nodes and 3 traditional Compute node which have been refurbished from Castor.
If you are using the internet inside NARIT network you can directly connect to these systems via the Secure shell (ssh) command.

Connection from outside NARIT network
========================================================

However, if you are using the internet outside NARIT, you need to log in to the gateway machine, A.K.A. stargate, first. The gateway machine’s IP address and other information are given to you once you get the permission to access the Chalawan Cluster.

Secure shell (ssh) through an intermediate host (the gateway)
===================================================================

This is the easiest method that using the ProxyJump directive. If this method doesn’t work for you because you are using the very old version of ssh, please read the next section.

To use ProxyJump, you can simply add the flag -J followed by user@gateway.ip:port. The example below shows how to connect to Castor (don’t forget to replace gateway.ip and port with the given information from the email).
::
    [user@local ~]$ ssh -J user@gateway.ip:port user@192.168.5.100

asasasa
