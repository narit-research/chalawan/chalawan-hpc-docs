==================
Getting an account
==================

To start using Chalawan Cluster, user must first submit an online application to get computing time credit and storage space. The merit of your proposal will also be considered, taking into account amount of requested resources. This will also provide our clusteradmin with a better understanding of what our users need and how best to prepare the environments and applications for you. 

Online application 
=========================================================

Sign Up
--------
Sign up for an `Online Application <http://chalawan.narit.or.th/application/index.php>`_ account. If you are eligible, your account should be activated within one working day. After the account is activated, please sign in to the system with the username (email address) and password you provided at the sign up and start filling the online application form.

Submit your application
-----------------------
Fill an `online application <http://chalawan.narit.or.th/application/tutorial.php>`_ form and submit. Once completed, you should receive notification of our decision within a week and further query regarding required software and setup if any.

Account setup notification
--------------------------
Once your account and required setup are ready, our system admin will send you an email regarding your account details, allocated :ref:`Slurm credit <slurm-credit-label>`, :ref:`storage <storage-label>` quota and how to login to Chalawan cluster.
