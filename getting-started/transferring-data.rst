=========================
Transferring Files & Data
=========================

rsync & scp (secure copy)
=========================================================

Jump host
-----------------

Multi-stage 
-----------------


Cloud Storage
=========================================================
It is possible to mount your cloud storage drive using `Rclone <https://rclone.org>`_. 


JupyterLab Interface
=========================================================
While accessing the cluster via `Chalawan JupyterLab <https://lab.narit.or.th>`_, user may use the interface to upload or download local file into or 
out off the cluster (see :ref:`Using Jupyerterlab <jupyterlab-label>`).  

.. Note::
     The upload filesize limit via JupyterLab interface is set to 100MB. User will be asked to confirm when trying to upload a file with size larger than 15MB. Please consider using altternative method to upload/download many large files as this would negatively affect connections of other users. 

