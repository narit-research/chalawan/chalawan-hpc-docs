===================================
Chalawan HPC Documentation
===================================
.. image:: https://readthedocs.org/projects/chalawan-hpc-docs/badge/?version=latest
   :target: https://chalawan-hpc-docs.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status

Chalawan is the high performance computing cluster that powered through the use of parallel programming, increasingly relies on
massive datasets and compute-intensive workloads with a wide range of applicability and deployment.


.. Getting Started section
.. toctree::
   :maxdepth: 1
   :caption: Quick Start Guide

   getting-started/getting-an-account
   getting-started/accessing-the-chalawan
   getting-started/connection-from-outside
   getting-started/transferring-data
   getting-started/using-module-environment
   getting-started/job-submission


.. Getting help section
.. toctree::
   :maxdepth: 1
   :caption: Chalawan

   chalawan/computing-resource
   chalawan/storage-resource
   chalawan/policy-queue
   chalawan/slurm-credit-allocation
   chalawan/acknowledgement-application


.. General stallo sections
.. toctree::
   :maxdepth: 1
   :caption: Running Jobs

   
   running-jobs/slurm-workload-manager
   running-jobs/running-parallel-jobs
   running-jobs/job-management
   running-jobs/slurm-cheatsheet


.. Account section
.. toctree::
   :maxdepth: 1
   :caption: Software

   software/which-software
   software/missing-new
   software/software-module
   software/python
   software/linux
   software/general

.. Job section
.. toctree::
   :maxdepth: 1
   :caption: Using JupyterLab

   using-jupyter-lab/using-jupyterlab



.. toctree::
   :maxdepth: 2
   :caption: Getting Help

   getting-help/contact-us
   getting-help/faq

