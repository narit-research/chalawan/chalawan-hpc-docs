==================
Job Management
==================

Job status
=========================================================
However, if there is no empty slot, the job will be listed in a pending (**PD**) state. You can view it by using the command ``squeue``. The output column **ST** stands for state. A running job is displayed with a state **R**. The Compute node where the job is running on is shown in the last column.
::
    [user@pollux]$ squeue
    JOBID PARTITION     NAME    USER ST    TIME NODES NODELIST(REASON)
    1587  chalawan_    task2    goku PD    0:00     1  (Priority)
    1585  chalawan_    task1    user PD    0:00     1  (Resources)
    1584  chalawan_    task0  vegeta R  3:21:49     1  pollux3

Job deletion
=========================================================
To remove a running job or a pending job from the queue, please use the command ``scancel`` followed by the job id.
To cancel all your jobs (running and pending) you can run  ``scancel -u <username>``.

Job history
=========================================================
``sacct`` displays accounting data for all jobs and job steps in the Slurm job accounting log or Slurm database.
