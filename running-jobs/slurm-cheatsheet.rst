==================
Slurm Cheatsheet
==================

All Slurm command started with ‘s’ followed by abbrevation of action word. Here we list the basic commands for submitting or deleting a job and query the information from it.

``sacct`` is used to report job or job step accounting information about active or completed jobs.

``salloc`` is used to allocate resources for a job in real time. Typically this is used to allocate resources and spawn a shell. The shell is then used to execute srun commands to launch parallel tasks.

``sbatch`` is used to submit a job script for later execution. The script will typically contain one or more srun commands to launch parallel tasks.

``scancel`` is used to cancel a pending or running job or job step. It can also be used to send an arbitrary signal to all processes associated with a running job or job step.

``sinfo`` reports the state of partitions and nodes managed by Slurm. It has a wide variety of filtering, sorting, and formatting options.

``squeue`` reports the state of jobs or job steps. It has a wide variety of filtering, sorting, and formatting options. By default, it reports the running jobs in priority order and then the pending jobs in priority order.
