==================
Basic Linux commands
==================
In this page, you can learn the basic commands of linux systems. 
It helps to work effectively in Linux.

File Management
=================

pwd
------
Find out what directory you are currently in.

cd
------
Move to another working directory

ls
------
List all files and directories.

cp
------
Copy file

scp
------
Copy file across systems

mv
------
Move file

rm
------
Remove file

tar
------
Extract and zip files


wget
------
Download a file via the internet


