============================================
Installing Software on the Chalawan HPC
============================================

If you find out your desirable software is not installed on our HPC system,
you can use these instructions to install it.

First thing that you must know before installing the software is 
which compilation tools or the system requirements
are used for installing/compling the software.
Then, you can follow these "general" steps to install your software.

1. Preparaing a software
----------------------------

You must upload/download your code into your desirable location, for example, 
you can download the software package via wget by typing, ::

    wget -p internet/sourcecode.tar.gz foldername


We suggest that you place it in the home folder (e.g., "/home/username/prgogram/").
If the software is archives, you can extract it into the specific folder by typing ::

    tar -zxvf sourcecode.tar.gz softwarefolder


2. Installing a software
----------------------------
Before starting to install a software, you must check the system requirements. 
If you download the source code from the developper, 
you can find the installation instructions in the package (e.g., README file).
You can load the the specific complier version (for example, gnu version 9.3.0) by typing, ::

    module load gnu9/9.3.0

Please note that, when you loaded the different version of softwares, it might be changed the complier version accordingly.
Therefore, plaese make sure that all pre-installing modules/compliers are matched.

To install a software, you first move the terminal into the software folder 
and then starting config the software by typing ::

    cd softwarefolder
    ./configure


Then, we compile all source codes by typing ::

    make
    make install


If all sources are complied correctly, 
you will find the new installed software file in that folder.


3. Loading a software
-------------------------------------

In order to easily use your installed software, we can load or make an alias by setting it in the bash script.

First, you open the bash script (.bashrc) located in your home folder "/home/username/.bashrc".

Then, you load your software and make an alias to call your software by adding this line into the ".bashrc" file, ::

    alias myprogram='softwarefolder/sotwarename'


where "myprogram" is the software alias that you can change and "softwarefolder/sotwarename" is the location of your software.
Note that, before setting an alias, you must check that this alias is avaliable first 
(ex., typing this alias in terminal).

Finally, you reload the bash script by typing (one time), ::

    source .bashrc

After you finished all theses steps, you can call your software via the alias by typing, ::

    myprogram















