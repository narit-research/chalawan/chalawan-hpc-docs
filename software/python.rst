===============================================
Python (Version, Environment and Packages) 
===============================================


Default Python version on the Chalawan HPC
----------------------------------------
Python is preinstalled on the Chalawan HPC clusters.
You can call the Python program by typing,

- Python version 2.x ::

    python


- Python version 3.x ::

    python3

It is the default program, and pre-installed some packages, for example, 
astropy, matplotlib, numpy, pandas, scipy and etc. 
You can easily run your code by typing, ::

    python myprogram.py
    python3 myprogram.py


Create your environment
----------------------------------------
Users may be needed to manage the packages which is not allowed with the default.
Therefore, if you want to manage the installed packaged, you should create your own environment (packages).
Note that, due to the limitation of free space in your home folder, 
you should recheck the freespace and cache files when you installed the new packages.

You can use the following steps to crate your own environment;

To load the Anaconda module to manage the environment, type, ::

    module load anaconda


To create your own environment, for example, the environtment name "myenv", type, ::

    conda create -n myenv


If you want to set a specific version of Python, type: ::

    conda create -n myenv python=3.7


When you needed to load your environment for coding or running your program, type, ::

    module load anaconda
    conda activate myenv



Managing Packages
----------------------------------------

We can use Anaconda to manage the Python packages.

To install the new package, type, ::

    conda install packagename

Or, via the "pip" module, ::

    pip install packagename

To uninstall the package, type, ::

    pip uninstall packagename



Packages that you installed may be upgradable for fixing some bugs and improving perfermence.

To update all packages of your environment, type, ::

    conda update -n myenv --update-all


Or, with a specific package, type, ::

    pip install packagename --upgrade








