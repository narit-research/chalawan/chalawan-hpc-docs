==================================================
Software Module Scheme & Module environment
==================================================

The Chalawan HPC is already pre-installed software packages for all users. 
To find out a list of software package that is available, type::
    module av


To check a list of software packages is currently loaded, 
type::
    module list

Load/Unload module
--------------------------
If you would like to change/swap the module, 
you can load /unload the module by typing::
    module load modulename
    module unload modulename



