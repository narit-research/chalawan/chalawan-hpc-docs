=========================================
Which software is installed on Chalawan 
=========================================

For all users on our system, the Chalawan HPC clusters come with a set of softwares preconfigured.
The software on the Chalawan HPC may be divided into two categories:

    - General software
    - Sharded licence software

Users can manage the preloaded software and compilation tools which are available 
on the Chalawan HPC system by using the "module" tool.

To list the loaded softwares, type, ::

    module list

If you find out your program is not avaliable in our HPC system, 
you can try to install your program by using complilation tools 
or contact our HPC staff.
