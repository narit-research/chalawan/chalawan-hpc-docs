=============================
First steps with Jupyterlab
=============================

Types of Cells
-----------------
- Code
- Markdown
- Raw

Run Cells
-----------------

type ::

    import numpy as np
    import matplotlib.pyplot as plt
    x = np.linspace(0.0, 2.0*np.pi, 100)
    y = np.sin(x)
    plt.plot(x, y)
    plt.show()



Magic Functions
-----------------
