=====================
JupyterLab Interface
=====================

Top Menu
--------------

Launcher
--------------

Terminal
--------------

Jupyter Notebook
-------------------


.. image:: /images/jupyterlab_interface1.png
   :width: 600

.. image:: /images/jupyterlab_interface2.png
   :width: 600

.. image:: /images/jupyterlab_interface3.png
   :width: 600

.. image:: /images/jupyterlab_interface4.png
   :width: 600